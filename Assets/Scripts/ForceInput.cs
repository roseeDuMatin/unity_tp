﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using static System.Linq.Enumerable;
using UnityEngine;
using UnityEngine.UI;

public class ForceInput : MonoBehaviour {

    // Use this for initialization
    public float ballspeed;
	public GUIStyle style;
	//public int score = -10;

	public int score
    {
		get
		{
			return scored.Where(_ => _ == true).Count() * 10;
		}
    }
	//private GameObject Can;
	private GameObject Barrel;
	private bool vert_test;
	private Quaternion localQ;
	private float fallTime = 0.0f;
    float angle = 0.0f;
    Vector3 axis = Vector3.zero;
    public float slider_v = 1;
    public Slider speed_slider;

	public Slider lines_slider;
	//public int number;

    public List<bool> scored = Enumerable.Empty<bool>().ToList();

	void OnGUI () {
		float x = Screen.width / 2f;
		float y = 30f;
		float width = 600f;
		float height = 200f;
		string text_speed = "Ball Speed: " + ballspeed.ToString();
		string text_score = "Score = " + score.ToString();
		style.fontSize = 30;
		
		GUI.Label(new Rect(x-(width/2f), y, width, height), text_speed, style);
		GUI.Label(new Rect(x-(width/2f), y+30f, width, height), text_score, style);

		bool isEnd = scored.Count > 0 && scored.All(_ => _ == true);
		if (isEnd)
		{
			string game_over = "GAME OVER, BRAVO !!";
			GUI.Label(new Rect(x-(width/2f), y+100f, width, height), game_over, style);
		}
	}

	public void Reset()
    {
		scored = Enumerable.Repeat(false, getNumber()).ToList();
	}

    public void slide_change (float value)
    {
        slider_v = value;
    }

	public void Start () {

        if (speed_slider)
            speed_slider.onValueChanged.AddListener(slide_change);

		scored = Enumerable.Repeat(false, getNumber()).ToList();
		fallTime = 3.0f;
    }

	void Update() {
		if (fallTime > 5.0f) {
			float roulette = (float)Input.GetAxisRaw ("Mouse ScrollWheel");
			ballspeed += roulette;

            if (speed_slider)
                ballspeed = slider_v;

			if (ballspeed < 0)
				ballspeed = 0;

			// for each Can: if moved enough from vertical, then add 10 to score and mark as moved
			Barrels barrels = GetComponent<Barrels>();
			for (int i = 0; i < getNumber(); i++) {
				Barrel = GameObject.Find("Barrel" + (i + 1).ToString());
				localQ = Barrel.transform.rotation;
                localQ.ToAngleAxis(out angle, out axis);

				float magRot = Mathf.Abs (localQ.eulerAngles.x) + Mathf.Abs (localQ.eulerAngles.z);

                vert_test = true;
				if (magRot > 10 && magRot < 350)
					vert_test = false;
				
				if (magRot < -10 && magRot > -350)
					vert_test = false;
				
				if (vert_test == false && scored[i] == false) {
					scored[i] = true;
				}
			}
		}
		fallTime += Time.deltaTime;
	}

	public int getNumber()
	{
		int numberPerLine = 1;
		int number = 1;
		for (int i = (int) this.lines_slider.value; i > 0; i--)
		{
			for (int j = 0; j < numberPerLine; j++)
			{
				number++;
			}
			numberPerLine++;
		}
		return number - 1;
	}
}

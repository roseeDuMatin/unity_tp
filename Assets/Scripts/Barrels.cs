using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Barrels : MonoBehaviour
{
    public int slider_v;
    public GameObject BarrelDefault;
    public Slider lines_slider;

    protected static float factorY = 1.9f;
    protected static float width = 1f;
    protected static float height = 1.15f;
    private float fallTime = 0.0f;

    [HideInInspector]
    public int Lines;

    void Start()
    {
        Lines = slider_v;
        fallTime = 3.0f;
    }

    public void Generate()
    {
        //DestroyAll();
        this.BarrelDefault.SetActive(false);
        GameObject Scene = GameObject.Find("Scene");

        int numberPerLine = 1;
        int number = 1;
        for (int i = this.Lines; i > 0; i--)
        {
            for (int j = 0; j < numberPerLine; j++)
            {
                GameObject Barrel = Instantiate(this.BarrelDefault, generateVector3(numberPerLine, j, i), Quaternion.identity);
                Barrel.name = "Barrel" + number.ToString();
                Barrel.transform.parent = Scene.transform;
                Barrel.SetActive(true);

                number++;
            }
            numberPerLine++;
        }
    }

    public void Reset()
    {
        DestroyAll(this.Lines);
        Generate();
    }

    private void DestroyAll(int lines)
    {
        int numberPerLine = 1;
        int number = 1;
        for (int i = lines; i > 0; i--)
        {
            for (int j = 0; j < numberPerLine; j++)
            {
                GameObject Barrel = GameObject.Find("Barrel" + number.ToString());
                Destroy(Barrel);

                number++;
            }
            numberPerLine++;
        }
    }

    void Update()
    {
        if (fallTime > 5.0f)
        {
            DestroyAll(Lines);
            Lines = slider_v;
            Generate();
        }
        fallTime += Time.deltaTime;
    }

    public void OnSlideChanged()
    {
        slider_v = (int) lines_slider.value;
    }

    private static float generateY(int line)
    {
        return height * line + factorY;
    }

    private static float generateX(int numberPerLine, int line)
    {
        float factor = numberPerLine % 2 == 0 ? 0f : -(0.5f * width);
        return (-0.5f * width * numberPerLine) + (line * width) + (0.5f * width);
    }

    private static Vector3 generateVector3(int numberPerLine, int line, int index)
    {
        return new Vector3(generateX(numberPerLine, line), generateY(index), 0f);
    }

    public int getLines()
    {
        return Lines;
    }

    public int getNumber()
    {
        int numberPerLine = 1;
        int number = 1;
        for (int i = this.Lines; i > 0; i--)
        {
            for (int j = 0; j < numberPerLine; j++)
            {
                number++;
            }
            numberPerLine++;
        }
        return number;
    }
}
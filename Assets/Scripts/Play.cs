using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Play : MonoBehaviour
{
    [HideInInspector]
    public bool showMenu = true;
    protected GameObject Canvas;
    protected GameObject Menu;

    void Start() {
        Menu = GameObject.Find("Menu");
        Canvas = GameObject.Find("Canvas");

        Show();
    }

    void Update() { }

    public void OnPlay()
    {
        showMenu = false;        
        Show();
    }

    public void OnMenu()
    {
        showMenu = true;
        Show();
    }

    private void Show()
    {
        Menu.SetActive(showMenu);
        Canvas.SetActive(!showMenu);
    }
}

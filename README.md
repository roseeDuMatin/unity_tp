# Chambouletout Sophie PELLUET 5MOC

## Description
Un chambouletout sur android avec un menu de sélection pour choisir le nombre de lignes de tonneaux à faire tomber.

La version 3D en réalité augmentée nécessite l'impression de l'image ci-dessous :

![Marker3D](Capture.PNG "Marker3D")